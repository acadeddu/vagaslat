nginx:
  pkg:
      - installed
  service:
      - running

/usr/share/nginx/html/index.html:                        # ID declaration
  file:                                     # state declaration
    - managed                               # function
    - source: salt://webserver/index.html   # function arg
    - require:                              # requisite declaration
      - pkg: nginx                          # requisite reference
