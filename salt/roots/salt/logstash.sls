include:
    - jre

{% set node_type = salt['grains.get']('node_type', '') %}

logstash:
    pkg.installed:
        - sources:
            - logstash: https://download.elasticsearch.org/logstash/logstash/packages/debian/logstash_1.4.2-1-2c0f5a1_all.deb
    service:
        - dead
        - watch:        
            - file: /etc/logstash/conf.d/{{ node_type[0] }}.conf
        - require:
            - pkg: logstash
            - user: logstash
    user:
        - present
        - fullname: Logstash User
        - home: /srv/logstash
        - system: true

# logstash-web:
#     service:
#         - running
#         - require:
#             - pkg: logstash
# /srv/logstash/:
#   file.directory:
#     - makedirs: True
#     - user: logstash
#     - group: logstash
#     - require: 
#         - user: logstash




/etc/logstash/conf.d/{{ node_type[0] }}.conf:
    file:
        - managed
        - source: salt://logstash/{{ node_type[0] }}.conf
    require:
        - pkg: logstash

/home/vagrant/{{ node_type[0] }}.sh:
    file:
        - managed
        - source: salt://logstash/{{ node_type[0] }}.sh
        - mode: 755
        - user: vagrant
    require:
        - pkg: logstash
#runs logstash agent and web
#sudo /opt/logstash/bin/logstash agent -f /etc/logstash/conf.d -l /var/log/logstash/logstash.log -- web
#    cmd.run