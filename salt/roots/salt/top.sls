base:
    '*':
        - hosts
        - jre
        - logstash
    'node_type:central':
        - match: grain
        - elasticsearch
        - redis
        # - logstash-web
    'node_type:shipper':
        - match: grain
        - nginx
        - node
