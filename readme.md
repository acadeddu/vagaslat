# vagaslat:
a simple toy vagrant box provisioned with salt, with nginx, node, elasticsearch and logstash.
## usage:
clone the repo, then run `vagrant up` ! It will magically install it all. you’ll have two boxes at the ip’s `192.168.1.15` (the central) and `192.168.1.20` (the shipper).
