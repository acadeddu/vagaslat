include:
    - jre

elasticsearch:
    pkg.installed:
       -  sources:
            - elasticsearch: https://download.elasticsearch.org/elasticsearch/elasticsearch/elasticsearch-1.2.1.deb
    service:
        - running
        - require:
            - pkg: elasticsearch
        - watch:
            - file: /etc/elasticsearch/elasticsearch.yml

/etc/elasticsearch/elasticsearch.yml:
    file:
        - managed
        - source: salt://logstash/elasticsearch.yml